# -*- coding: utf-8 -*-

# Hostname without dot end
# NO: hostname.com.
# YES: hostname.com
HOSTNAME = "lesshms.com"
SERVER_IP = "62.109.6.167"

HOSTER_NS1 = "ns1.firstvds.ru"
HOSTER_NS2 = "ns2.firstvds.ru"

NS1 = "ns1.%s" % HOSTNAME
NS2 = "ns2.%s" % HOSTNAME

SHELL = "/bin/bash"

UID_START = 2000
GID_START = 2000