#!/bin/bash
apt-get update
apt-get install gcc
apt-get install nginx
apt-get install python
apt-get install python-dev
apt-get install python-virtualenv
apt-get install build-essential
apt-get install uwsgi
apt-get install uwsgi-plugin-python
mkdir /var/www/virtualenv
virtualenv virtualenv/lesshms
source virtualenv/lesshms/bin/activate
cd lesshms
pip install -r requirements.txt
python manage.py syncdb
python manage.py migrate
echo "LessHMS is installed"