# -*- coding: utf-8 -*-
import os

from django.db import models
from django.db.models.signals import post_delete, post_save
from django.template.loader import render_to_string

from lesshms.paths import BIND
from lesshms.server_conf import NS1, NS2, HOSTNAME, SERVER_IP, HOSTER_NS1, HOSTER_NS2
from apps.domains.signals import change_domain_list
from libs.fields import DomainNameField


class Domain(models.Model):
    name = DomainNameField(u"Domain", max_length=255, help_text=u"example.com", unique=True)
    is_root = models.BooleanField(u"Is root domain", default=False)

    class Meta:
        verbose_name = u"Domain"
        verbose_name_plural = u"Domains"
        ordering = ["name"]

    def __unicode__(self):
        return self.get_normal_name()

    def get_normal_name(self):
        return self.name.decode("idna")

    def get_name_dot(self):
        """
        A dot at the end.
        example.com -> example.com.
        """
        name = self.get_normal_name()

        if not name.endswith("."):
            return u"%s." % name
        return name

    def get_name_without_dot(self):
        """
        A dot at the end.
        example.com.. -> example.com
        """
        name = self.name

        while name.endswith("."):
            name = name[:-1]
        return name

    def init_dns(self):
        """
        Create initial DNS records for domain.
        """

        DNS.objects.filter(domain=self).delete()

        # SOA
        DNS(
            domain=self,
            dns_type="SOA",
            value="%s. root.%s. (2013092600 10800 3600 604800 86400)" % (HOSTNAME, HOSTNAME)
        ).save()

        # NS
        ns1 = HOSTER_NS1 if self.is_root else NS1
        ns2 = HOSTER_NS2 if self.is_root else NS2

        DNS(domain=self, dns_type="NS", sub_domain="%s." % ns1).save()
        DNS(domain=self, dns_type="NS", sub_domain="%s." % ns2).save()

        # MX
        DNS(domain=self, dns_type="MX", priority=10, mx_server="mail").save()
        DNS(domain=self, dns_type="MX", priority=20, mx_server="mail").save()

        DNS(domain=self, dns_type="A", value=SERVER_IP).save()

    def save_dns(self):
        """
        Create DNS file in Bind directory.
        """

        dns = open(os.path.join(BIND, self.get_name_without_dot()), "w+")
        dns_template = render_to_string("domains/conf/dns.html", {
            "domain": self,
            "SERVER_IP": SERVER_IP,
            "dns": self.dns_set.all().order_by("id")
        })
        dns.write(dns_template)

        dns.close()


DNS_TYPES = [
    ["MX", "MX"],
    ["SOA", "SOA"],
    ["NS", "NS"],
    ["A", "A"],
    ["CNAME", "CNAME"]
]


class DNS(models.Model):
    domain = models.ForeignKey(Domain, verbose_name=u"domain", on_delete=models.CASCADE)
    dns_type = models.CharField(u"DNS type", choices=DNS_TYPES, max_length=10, null=False)

    sub_domain = models.CharField(u"sub domain", max_length=100, blank=True)
    mx_server = models.CharField(u"MX server", max_length=100, blank=True)
    priority = models.PositiveIntegerField(u"priority", null=True, default=None)
    value = models.CharField(u"value", blank=True, default="", max_length=100)

    class Meta:
        verbose_name = u"DNS"
        verbose_name_plural = u"DNS"

    def __unicode__(self):
        return u"DNS for %s" % self.domain.name

    def get_domain(self):
        if self.dns_type == "MX":
            if self.sub_domain:
                return self.sub_domain
        return self.domain.get_name_dot()

    def get_value(self):
        if self.dns_type == "MX":
            return "%s %s" % (self.priority, self.mx_server)

        if self.dns_type in ["A", "SOA"]:
            return self.value

        if self.dns_type == "NS":
            return self.sub_domain

        return ""


post_save.connect(change_domain_list, sender=Domain)
post_delete.connect(change_domain_list, sender=Domain)