# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404
from django.template import RequestContext
from django.template.loader import get_template
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.contrib import messages

from apps.domains.models import Domain, DNS
from apps.domains.forms import DomainForm, MXRecordForm


@login_required
def domain_list(request):
    domains = Domain.objects.all()

    template = get_template("domains/domain_list.html")
    context = RequestContext(request, {
        "mm_item": "domains",
        "html_title": u"Domains",
        "domains": domains
    })
    return HttpResponse(template.render(context))


@login_required
def domain_ac(request, domain_id=None):

    domain_dict = None
    if domain_id:
        domain = get_object_or_404(Domain, id=domain_id)
        domain_dict = {
            "name": domain.get_normal_name(),
            "is_root": domain.is_root
        }
    else:
        domain = Domain()

    form = DomainForm(domain_id, request.POST or None, initial=domain_dict, )

    if request.method == "POST":
        if form.is_valid():
            domain.name = form.cleaned_data["name"]
            domain.is_root = form.cleaned_data["is_root"]
            domain.save()

            messages.info(request, 'Domain %s is %s.' % (domain.name, "changed" if domain_id else "added"))

            return HttpResponseRedirect(reverse("domains:all"))

    template = get_template("domains/domain_ac.html")
    context = RequestContext(request, {
        "mm_item": "domains",
        "html_title": u"Add domain" if not domain_id else u"Change domain",

        "breadcrumbs": [
            {"name": u"Domains", "url": reverse("domains:all")},
        ],

        "form": form,
        "change": domain_id
    })
    return HttpResponse(template.render(context))


@login_required
def domain_delete(request, domain_id):
    domain = get_object_or_404(Domain, id=domain_id)
    domain_name = domain.get_normal_name()
    domain.delete()

    messages.info(request, 'Domain %s deleted' % domain_name)

    return HttpResponseRedirect(reverse("domains:all"))


@login_required
def domain_dns(request, domain_id):
    domain = get_object_or_404(Domain, id=domain_id)

    dns = domain.dns_set.all().order_by("id")

    template = get_template("domains/domain_dns.html")
    context = RequestContext(request, {
        "mm_item": "domains",
        "html_title": u"DNS %s" % domain.get_normal_name(),

        "breadcrumbs": [
            {"name": u"Domains", "url": reverse("domains:all")},
            {"name": domain.get_normal_name()},
            {"name": "DNS"},
        ],

        "dns": dns,
        "domain": domain
    })
    return HttpResponse(template.render(context))


@login_required
def domain_dns_ac(request, domain_id, dns_id=None):
    """
    Add/Change DNS Record.
    Only MX record.
    """
    domain = get_object_or_404(Domain, id=domain_id)

    dns_data = {
        "dns_type": "MX"
    }
    if dns_id:
        dns = get_object_or_404(DNS, id=dns_id, domain=domain, dns_type="MX")

        dns_data = {
            "sub_domain": dns.sub_domain or domain.get_name_dot(),
            "mx_server": dns.mx_server,
            "priority": dns.priority,
            "dns_type": dns.dns_type
        }
    else:
        dns = DNS(domain=domain)

    form = MXRecordForm(request.POST or None, initial=dns_data)

    if request.method == "POST":
        if form.is_valid():
            for field in ["sub_domain", "mx_server", "priority", "dns_type"]:
                setattr(dns, field, form.cleaned_data[field])
            dns.save()
            domain.save_dns()

            messages.info(request, 'DNS record %s for domain %s %s' % (
                dns.dns_type,
                domain.get_normal_name(),
                "changed" if dns_id else "added"
            ))

            return HttpResponseRedirect(reverse("domains:dns", kwargs={"domain_id": domain.id}))

    template = get_template("domains/domain_dns_ac.html")
    context = RequestContext(request, {
        "mm_item": "domains",
        "html_title": u"Domain DNS",

        "breadcrumbs": [
            {"name": u"Domains", "url": reverse("domains:all")},
            {"name": domain.get_normal_name()},
            {"name": "DNS", "url": reverse('domains:dns', kwargs={"domain_id": domain.id})}
        ],

        "dns": dns,
        "domain": domain,
        "form": form,
        "change": dns_id
    })
    return HttpResponse(template.render(context))


@login_required
def domain_dns_delete(request, domain_id, dns_id=None):

    domain = get_object_or_404(Domain, id=domain_id)
    dns = get_object_or_404(DNS, id=dns_id, domain=domain, dns_type="MX")
    dns_type = dns.dns_type
    dns.delete()

    messages.info(request, 'DNS record %s for domain %s deleted' % (
        dns_type,
        domain.get_normal_name()
    ))

    return HttpResponseRedirect(reverse("domains:dns", kwargs={"domain_id": domain.id}))
