# -*- coding: utf-8 -*-
from django.template.loader import render_to_string

from lesshms.paths import BIND_NAMED_CONF_LOCAL


def change_domain_list(sender, instance, **kwargs):
    """
    Added, chended or deleted domains.
    """
    from apps.domains.models import Domain

    created = kwargs.get("created", None)

    # Rewrite named.conf.local
    named_conf_local = open(BIND_NAMED_CONF_LOCAL, "w+")

    for d in Domain.objects.all():
        named_conf_item = render_to_string("domains/conf/named_conf_item.html", {"domain": d})
        named_conf_local.write(named_conf_item)
    named_conf_local.close()

    # Default DNS
    if created:
        instance.init_dns()
        instance.save_dns()

    # Deleted
    if created is None:
        # TODO: delete DNS file
        pass