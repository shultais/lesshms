# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'DNS'
        db.create_table(u'domains_dns', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('domain', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['domains.Domain'])),
            ('dns_type', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('sub_domain', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('mx_server', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('priority', self.gf('django.db.models.fields.PositiveIntegerField')(default=None, null=True)),
        ))
        db.send_create_signal(u'domains', ['DNS'])


    def backwards(self, orm):
        # Deleting model 'DNS'
        db.delete_table(u'domains_dns')


    models = {
        u'domains.dns': {
            'Meta': {'object_name': 'DNS'},
            'dns_type': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'domain': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['domains.Domain']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mx_server': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'priority': ('django.db.models.fields.PositiveIntegerField', [], {'default': 'None', 'null': 'True'}),
            'sub_domain': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        u'domains.domain': {
            'Meta': {'object_name': 'Domain'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['domains']