# -*- coding: utf-8 -*-

from django import forms
from libs.validators import DomainNameValidator

# TODO need to create Field and Widget for class=form-control.


class DomainForm(forms.Form):
    name = forms.CharField(
        label=u"Domain name",
        required=True,
        widget=forms.TextInput(attrs={
            "class": "form-control",
            "placeholder": "For example, domain.com"
        }),
        validators=[DomainNameValidator]
    )

    is_root = forms.BooleanField(
        label=u"Use Hoster NS",
        required=False,
    )

    def __init__(self, domain_id, *args, **kwargs):
        super(DomainForm, self).__init__(*args, **kwargs)
        self.domain_id = domain_id

    def clean_name(self):
        from apps.domains.models import Domain

        name = self.cleaned_data.get('name').strip().lower()

        # Check unique
        if Domain.objects.filter(name=name).exclude(id=self.domain_id).exists():
            raise forms.ValidationError(u"The domain %s already exists" % name)
        return name


# DNS forms
class MXRecordForm(forms.Form):
    dns_type = forms.CharField(
        widget=forms.HiddenInput()
    )
    priority = forms.IntegerField(
        label=u"Priority",
        required=True,
        # In fact, the value can take 0, but I use PositiveIntegerField:)
        min_value=1,
        max_value=65535,
        widget=forms.TextInput(attrs={
            "class": "form-control"
        }),
    )
    mx_server = forms.CharField(
        label=u"MX server",
        required=True,
        widget=forms.TextInput(attrs={
            "class": "form-control"
        })
    )
    sub_domain = forms.CharField(
        label=u"Subdomain",
        required=True,
        widget=forms.TextInput(attrs={
            "class": "form-control"
        })
    )