# -*- coding: utf-8 -*-

from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse

@login_required
def dashboard(request):
    return HttpResponseRedirect(reverse("domains:all"))
