# -*- coding: utf-8 -*-

from django import forms

from lesshms import DJANGO_VERSIONS, SOUTH_VERSIONS, PIL_VERSIONS

from apps.domains.models import Domain
from apps.users.models import LinuxUser

DJ_VERSIONS = [["", "------"]]
DJ_VERSIONS.extend([[version, version] for version in DJANGO_VERSIONS])

SH_VERSIONS = [["", "------"]]
SH_VERSIONS.extend([[version, version] for version in SOUTH_VERSIONS])

PL_VERSIONS = [["", "------"]]
PL_VERSIONS.extend([[version, version] for version in PIL_VERSIONS])


class ProjectForm(forms.Form):
    name = forms.CharField(
        label=u"Project name",
        required=True,
        max_length=100,
        widget=forms.TextInput(attrs={
            "class": "form-control",
            "placeholder": "For example, My super site"
        })
    )

    directory = forms.CharField(
        label=u"Directory name",
        required=True,
        max_length=100,
        widget=forms.TextInput(attrs={
            "class": "form-control",
            "placeholder": "For example, super_site"
        }),
        help_text=u"The directory name will be used to create a virtual environment "
                  u"and <strong>django project dir</strong>."
    )

    user = forms.ModelChoiceField(
        label=u"User for access",
        queryset=LinuxUser.objects.all().order_by("username"),
        required=True,
        widget=forms.Select(attrs={
            "class": "form-control"
        })
    )

    create_user = forms.BooleanField(
        label=u"Create new user",
        required=False,
    )

    username = forms.CharField(
        label=u"New Linux username",
        required=True,
        max_length=32,
        widget=forms.TextInput(attrs={
            "class": "form-control",
            "placeholder": "For example, lesshms",
            "disabled": "disabled"
        }),
        help_text=u"The short and simple name to log on SSH."
    )

    password = forms.CharField(
        label=u"Password",
        required=True,
        max_length=32,
        widget=forms.TextInput(attrs={
            "class": "form-control",
            "disabled": "disabled"
        }),
        help_text=u"A strong password."
    )

    domain = forms.ModelChoiceField(
        label=u"Domain",
        queryset=Domain.objects.all().order_by("name"),
        required=True,
        widget=forms.Select(attrs={
            "class": "form-control"
        })
    )

    # Packages
    django_version = forms.ChoiceField(
        required=False,
        label="Django",
        choices=DJ_VERSIONS,
        widget=forms.Select(attrs={
            "class": "form-control"
        })
    )

    south_version = forms.ChoiceField(
        required=False,
        label="South",
        choices=SH_VERSIONS,
        widget=forms.Select(attrs={
            "class": "form-control"
        })
    )

    pil_version = forms.ChoiceField(
        required=False,
        label="PIL",
        choices=PL_VERSIONS,
        widget=forms.Select(attrs={
            "class": "form-control"
        })
    )


class NewPackageForm(forms.Form):
    package = forms.CharField(
        label=u"Package",
        required=True,
        max_length=100,
        widget=forms.TextInput(attrs={
            "class": "form-control"
        })
    )

    version = forms.CharField(
        label=u"Version",
        required=True,
        max_length=20,
        widget=forms.TextInput(attrs={
            "class": "form-control"
        })
    )


class NignxForm(forms.Form):
    nginx_port = forms.IntegerField(
        label=u"Port",
        required=True,
        widget=forms.TextInput(attrs={
            "class": "form-control"
        })
    )

    nginx_enable_access_log = forms.BooleanField(
        label=u"Enable access log",
        required=False,
    )

    nginx_enable_error_log = forms.BooleanField(
        label=u"Enable error log",
        required=False,
    )

    nginx_static_root = forms.CharField(
        label=u"Static dir",
        required=False,
        widget=forms.TextInput(attrs={
            "class": "form-control"
        }),
        help_text="Relative path to static dir. <strong>static</strong> or <strong>project_name/static</strong>"
    )

    nginx_static_subdomain = forms.CharField(
        label=u"Static subdomain",
        required=False,
        widget=forms.TextInput(attrs={
            "class": "form-control"
        }),
        help_text="<strong>static</strong> for example."
    )

    nginx_static_url = forms.CharField(
        label=u"Static URL",
        required=False,
        widget=forms.TextInput(attrs={
            "class": "form-control"
        }),
        help_text="<strong>/static/</strong> for example. "
    )


class UwsgiForm(forms.Form):
    uwsgi_max_requests = forms.IntegerField(
        label=u"Max requests",
        required=True,
        widget=forms.TextInput(attrs={
            "class": "form-control"
        })
    )

    uwsgi_buffer_size = forms.IntegerField(
        label=u"Buffer size",
        required=True,
        widget=forms.TextInput(attrs={
            "class": "form-control"
        })
    )

    uwsgi_harakiri = forms.IntegerField(
        label=u"Harakiri",
        required=True,
        widget=forms.TextInput(attrs={
            "class": "form-control"
        })
    )

    uwsgi_settings_path = forms.CharField(
        label=u"Path to settings.py",
        required=False,
        widget=forms.TextInput(attrs={
            "class": "form-control"
        }),
        help_text="Relative python.path to settings. <strong>settings</strong> or <strong>project_name.settings</strong>"
    )