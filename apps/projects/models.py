# -*- coding: utf-8 -*-
import os
import shutil

from django.db import models
from django.template.loader import render_to_string
from django.conf import settings

from lesshms.paths import HOME, NGINX_SE, UWSGI_PATH, NGINX_UWSGI
from lesshms import PROJECT_DIRECTORY_ABC
from libs import touch
from apps.domains.models import Domain
from apps.users.models import LinuxUser


class Project(models.Model):
    user = models.ForeignKey(LinuxUser, default=None, null=True)
    directory = models.CharField(u"dirctory", max_length=100, default="")
    name = models.CharField(u"name", max_length=100)
    domain = models.ForeignKey(Domain, verbose_name=u"domain", null=False)

    # Nginx params
    nginx_port = models.PositiveIntegerField(default=80)

    nginx_enable_access_log = models.BooleanField(default=True)
    nginx_enable_error_log = models.BooleanField(default=True)

    nginx_static_root = models.CharField(max_length=100, default="", help_text="static or project_name/static")
    nginx_static_subdomain = models.CharField(max_length=100, default="")
    nginx_static_url = models.CharField(max_length=100, default="", help_text="/static/")

    # Uwsgi params
    uwsgi_settings_path = models.CharField(max_length=100, default="", help_text="settings or project_name.settings")
    uwsgi_max_requests = models.PositiveIntegerField(default=5000)
    uwsgi_buffer_size = models.PositiveIntegerField(default=32768)
    uwsgi_harakiri = models.PositiveIntegerField(default=30)

    class Meta:
        verbose_name = u"project"
        verbose_name_plural = u"projects"
        ordering = ["name"]

    def __unicode__(self):
        return self.human_name

    def save_nginx_config(self):
        """
        Create and Save nginx config.
        """

        # Create dirs
        nginx_dir = os.path.join(HOME, self.user.username, "nginx")
        nginx_logs = os.path.join(nginx_dir, "logs", self.directory)

        if not os.path.exists(nginx_dir):
            os.makedirs(nginx_dir)

        if not os.path.exists(nginx_logs):
            os.makedirs(nginx_logs)

        nginx_conf = os.path.join(nginx_dir, self.domain.get_name_without_dot())

        nginx_conf_file = open(nginx_conf, "w+")
        nginx_template = render_to_string("projects/conf/nginx.html", {
            "project": self,
            "nginx_logs": nginx_logs
        })

        nginx_conf_file.write(nginx_template)
        nginx_conf_file.close()

        # Simlink
        simlink = os.path.join(NGINX_SE, self.domain.get_name_without_dot())
        if not os.path.exists(simlink):
            os.symlink(nginx_conf, simlink)

        # Uwsgi params
        if not os.path.exists(NGINX_UWSGI):
            shutil.copyfile(os.path.join(settings.PROJECT_PATH, "lesshms", "uwsgi_params"), NGINX_UWSGI)

        if not settings.DEVELOP:
            os.chown(nginx_conf, int(self.user.uid()), int(self.user.gid()))

    def save_uwsgi_config(self):
        """
        Create and Save uwsgi config.
        """

        # Create dirs
        uwsgi_dir = os.path.join(HOME, self.user.username, "uwsgi")
        uwsgi_logs = os.path.join(uwsgi_dir, "logs", self.directory)

        if not os.path.exists(uwsgi_dir):
            os.makedirs(uwsgi_dir)

        if not os.path.exists(uwsgi_logs):
            os.makedirs(uwsgi_logs)

        uwsgi_conf = os.path.join(uwsgi_dir, "%s.ini" % self.directory)

        uwsgi_conf_file = open(uwsgi_conf, "w+")
        uwsgi_template = render_to_string("projects/conf/uwsgi.html", {
            "project": self
        })

        uwsgi_conf_file.write(uwsgi_template)
        uwsgi_conf_file.close()

        if not os.path.exists(self.get_uwsgi_touch()):
            touch_file = open(self.get_uwsgi_touch(), "w+")
            touch_file.close()
        touch(self.get_uwsgi_touch())

        if not settings.DEVELOP:
            os.chown(uwsgi_conf, int(self.user.uid()), int(self.user.gid()))
            os.chown(self.get_uwsgi_touch(), int(self.user.uid()), int(self.user.gid()))

    def get_project_root(self):
        subdirs = ["projects", self.directory]

        project_root = os.path.join(HOME, self.user.username)
        for subdir in subdirs:
            project_root = os.path.join(project_root, subdir)
            if not os.path.exists(project_root):
                os.makedirs(project_root)
                if not settings.DEVELOP:
                    os.chown(project_root, int(self.user.uid()), int(self.user.gid()))

        return project_root

    def get_static_root(self):
        return os.path.join(HOME, self.user.username, "projects", self.directory, self.nginx_static_root)

    def get_sock_path(self):
        # unix:///var/run/uwsgi/<directory>/<directory>.sock;

        # Create dirs
        # TODO: may not need to create a directory
        if not settings.DEVELOP:
            if not os.path.exists(UWSGI_PATH):
                os.makedirs(UWSGI_PATH)

            sock_path = UWSGI_PATH.replace("unix://", "")

            subdirs = [self.directory]
            for subdir in subdirs:
                sock_path = os.path.join(sock_path, subdir)
                if not os.path.exists(sock_path):
                    os.makedirs(sock_path)
                    if not settings.DEVELOP:
                        os.chown(sock_path, int(self.user.uid()), int(self.user.gid()))

            sock_path = os.path.join(sock_path, "%s.sock" % self.directory)

            # Create file
            if not os.path.exists(sock_path):
                sock_file = open(sock_path, "w+")
                sock_file.close()
                if not settings.DEVELOP:
                    os.chown(sock_path, int(self.user.uid()), int(self.user.gid()))

        return os.path.join(UWSGI_PATH, self.directory, "%s.sock" % self.directory)

    def get_sock_path_for_uwsgi(self):
        return self.get_sock_path().replace("unix://", "")

    def get_virtualenv_dir(self):
        # /home/<username>/virtualenv/<directory>/;
        return os.path.join(HOME, self.user.username, "virtualenv", self.directory)

    def get_uwsgi_log_dir(self):
        # /home/bodysport/uwsgi/logs/bodysport/bodysport.uwsgi.log
        return os.path.join(HOME, self.user.username, "uwsgi", "logs", self.directory)

    def get_uwsgi_touch(self):
        # /home/bodysport/uwsgi/logs/bodysport/bodysport.uwsgi.log
        return os.path.join(HOME, self.user.username, "uwsgi", "%s.touch" % self.directory)

    def get_uwsgi_conf(self):
        # /home/bodysport/uwsgi/logs/bodysport/bodysport.uwsgi.log
        return os.path.join(HOME, self.user.username, "uwsgi", "%s.ini" % self.directory)

    def get_nginx_conf(self):
        nginx_dir = os.path.join(HOME, self.user.username, "nginx")
        nginx_conf = os.path.join(nginx_dir, self.domain.get_name_without_dot())
        return nginx_conf

    @staticmethod
    def normalize_directory_name(name):
        name = name.strip().lower()
        new_name = ""
        for c in name:
            if c in PROJECT_DIRECTORY_ABC:
                new_name += c
        return new_name


class Package(models.Model):
    project = models.ForeignKey(Project)
    package = models.CharField(max_length=100)
    version = models.CharField(max_length=20)

    class Meta:
        verbose_name = u"package"
        verbose_name_plural = u"package"
        ordering = ["package"]
