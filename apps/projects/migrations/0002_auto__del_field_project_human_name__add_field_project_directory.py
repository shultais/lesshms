# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Project.human_name'
        db.delete_column(u'projects_project', 'human_name')

        # Adding field 'Project.directory'
        db.add_column(u'projects_project', 'directory',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Project.human_name'
        db.add_column(u'projects_project', 'human_name',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100),
                      keep_default=False)

        # Deleting field 'Project.directory'
        db.delete_column(u'projects_project', 'directory')


    models = {
        u'domains.domain': {
            'Meta': {'ordering': "['name']", 'object_name': 'Domain'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('libs.fields.DomainNameField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'projects.project': {
            'Meta': {'ordering': "['name']", 'object_name': 'Project'},
            'directory': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'domain': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['domains.Domain']", 'unique': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['projects']