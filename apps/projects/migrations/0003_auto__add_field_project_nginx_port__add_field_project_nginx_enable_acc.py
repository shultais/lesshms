# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Project.nginx_port'
        db.add_column(u'projects_project', 'nginx_port',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=80),
                      keep_default=False)

        # Adding field 'Project.nginx_enable_access_log'
        db.add_column(u'projects_project', 'nginx_enable_access_log',
                      self.gf('django.db.models.fields.BooleanField')(default=True),
                      keep_default=False)

        # Adding field 'Project.nginx_enable_error_log'
        db.add_column(u'projects_project', 'nginx_enable_error_log',
                      self.gf('django.db.models.fields.BooleanField')(default=True),
                      keep_default=False)

        # Adding field 'Project.nginx_static_root'
        db.add_column(u'projects_project', 'nginx_static_root',
                      self.gf('django.db.models.fields.BooleanField')(default=False, max_length=100),
                      keep_default=False)

        # Adding field 'Project.nginx_static_subdomain'
        db.add_column(u'projects_project', 'nginx_static_subdomain',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100),
                      keep_default=False)

        # Adding field 'Project.nginx_static_url'
        db.add_column(u'projects_project', 'nginx_static_url',
                      self.gf('django.db.models.fields.BooleanField')(default=False, max_length=100),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Project.nginx_port'
        db.delete_column(u'projects_project', 'nginx_port')

        # Deleting field 'Project.nginx_enable_access_log'
        db.delete_column(u'projects_project', 'nginx_enable_access_log')

        # Deleting field 'Project.nginx_enable_error_log'
        db.delete_column(u'projects_project', 'nginx_enable_error_log')

        # Deleting field 'Project.nginx_static_root'
        db.delete_column(u'projects_project', 'nginx_static_root')

        # Deleting field 'Project.nginx_static_subdomain'
        db.delete_column(u'projects_project', 'nginx_static_subdomain')

        # Deleting field 'Project.nginx_static_url'
        db.delete_column(u'projects_project', 'nginx_static_url')


    models = {
        u'domains.domain': {
            'Meta': {'ordering': "['name']", 'object_name': 'Domain'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('libs.fields.DomainNameField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'projects.project': {
            'Meta': {'ordering': "['name']", 'object_name': 'Project'},
            'directory': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'domain': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['domains.Domain']", 'unique': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'nginx_enable_access_log': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'nginx_enable_error_log': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'nginx_port': ('django.db.models.fields.PositiveIntegerField', [], {'default': '80'}),
            'nginx_static_root': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'max_length': '100'}),
            'nginx_static_subdomain': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'nginx_static_url': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'max_length': '100'})
        }
    }

    complete_apps = ['projects']