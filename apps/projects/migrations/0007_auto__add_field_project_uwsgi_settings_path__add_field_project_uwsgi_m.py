# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Project.uwsgi_settings_path'
        db.add_column(u'projects_project', 'uwsgi_settings_path',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100),
                      keep_default=False)

        # Adding field 'Project.uwsgi_max_requests'
        db.add_column(u'projects_project', 'uwsgi_max_requests',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=5000),
                      keep_default=False)

        # Adding field 'Project.uwsgi_buffer_size'
        db.add_column(u'projects_project', 'uwsgi_buffer_size',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=32768),
                      keep_default=False)

        # Adding field 'Project.uwsgi_harakiri'
        db.add_column(u'projects_project', 'uwsgi_harakiri',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=30),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Project.uwsgi_settings_path'
        db.delete_column(u'projects_project', 'uwsgi_settings_path')

        # Deleting field 'Project.uwsgi_max_requests'
        db.delete_column(u'projects_project', 'uwsgi_max_requests')

        # Deleting field 'Project.uwsgi_buffer_size'
        db.delete_column(u'projects_project', 'uwsgi_buffer_size')

        # Deleting field 'Project.uwsgi_harakiri'
        db.delete_column(u'projects_project', 'uwsgi_harakiri')


    models = {
        u'domains.domain': {
            'Meta': {'ordering': "['name']", 'object_name': 'Domain'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('libs.fields.DomainNameField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'projects.package': {
            'Meta': {'ordering': "['package']", 'object_name': 'Package'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'package': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'project': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['projects.Project']"}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'projects.project': {
            'Meta': {'ordering': "['name']", 'object_name': 'Project'},
            'directory': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'domain': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['domains.Domain']", 'unique': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'nginx_enable_access_log': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'nginx_enable_error_log': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'nginx_port': ('django.db.models.fields.PositiveIntegerField', [], {'default': '80'}),
            'nginx_static_root': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'nginx_static_subdomain': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'nginx_static_url': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': u"orm['users.LinuxUser']", 'null': 'True'}),
            'uwsgi_buffer_size': ('django.db.models.fields.PositiveIntegerField', [], {'default': '32768'}),
            'uwsgi_harakiri': ('django.db.models.fields.PositiveIntegerField', [], {'default': '30'}),
            'uwsgi_max_requests': ('django.db.models.fields.PositiveIntegerField', [], {'default': '5000'}),
            'uwsgi_settings_path': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'})
        },
        u'users.linuxgroup': {
            'Meta': {'ordering': "['name']", 'object_name': 'LinuxGroup'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_system': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'users.linuxuser': {
            'Meta': {'ordering': "['username']", 'object_name': 'LinuxUser'},
            'default_group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.LinuxGroup']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_system': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'salt': ('django.db.models.fields.CharField', [], {'default': "'salt33'", 'max_length': '6'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        }
    }

    complete_apps = ['projects']