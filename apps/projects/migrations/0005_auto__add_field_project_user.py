# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Project.user'
        db.add_column(u'projects_project', 'user',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=None, to=orm['users.LinuxUser'], null=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Project.user'
        db.delete_column(u'projects_project', 'user_id')


    models = {
        u'domains.domain': {
            'Meta': {'ordering': "['name']", 'object_name': 'Domain'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('libs.fields.DomainNameField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'projects.package': {
            'Meta': {'ordering': "['package']", 'object_name': 'Package'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'package': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'project': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['projects.Project']"}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'projects.project': {
            'Meta': {'ordering': "['name']", 'object_name': 'Project'},
            'directory': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'domain': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['domains.Domain']", 'unique': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'nginx_enable_access_log': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'nginx_enable_error_log': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'nginx_port': ('django.db.models.fields.PositiveIntegerField', [], {'default': '80'}),
            'nginx_static_root': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'max_length': '100'}),
            'nginx_static_subdomain': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'nginx_static_url': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'max_length': '100'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': u"orm['users.LinuxUser']", 'null': 'True'})
        },
        u'users.linuxgroup': {
            'Meta': {'ordering': "['name']", 'object_name': 'LinuxGroup'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_system': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'users.linuxuser': {
            'Meta': {'ordering': "['username']", 'object_name': 'LinuxUser'},
            'default_group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.LinuxGroup']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_system': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'salt': ('django.db.models.fields.CharField', [], {'default': "'salt33'", 'max_length': '6'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        }
    }

    complete_apps = ['projects']