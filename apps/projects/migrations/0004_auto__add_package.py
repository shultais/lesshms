# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Package'
        db.create_table(u'projects_package', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('project', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['projects.Project'])),
            ('package', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('version', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal(u'projects', ['Package'])


    def backwards(self, orm):
        # Deleting model 'Package'
        db.delete_table(u'projects_package')


    models = {
        u'domains.domain': {
            'Meta': {'ordering': "['name']", 'object_name': 'Domain'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('libs.fields.DomainNameField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'projects.package': {
            'Meta': {'ordering': "['package']", 'object_name': 'Package'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'package': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'project': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['projects.Project']"}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'projects.project': {
            'Meta': {'ordering': "['name']", 'object_name': 'Project'},
            'directory': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'domain': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['domains.Domain']", 'unique': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'nginx_enable_access_log': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'nginx_enable_error_log': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'nginx_port': ('django.db.models.fields.PositiveIntegerField', [], {'default': '80'}),
            'nginx_static_root': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'max_length': '100'}),
            'nginx_static_subdomain': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'nginx_static_url': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'max_length': '100'})
        }
    }

    complete_apps = ['projects']