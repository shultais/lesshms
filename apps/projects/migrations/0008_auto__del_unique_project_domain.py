# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Removing unique constraint on 'Project', fields ['domain']
        db.delete_unique(u'projects_project', ['domain_id'])


    def backwards(self, orm):
        # Adding unique constraint on 'Project', fields ['domain']
        db.create_unique(u'projects_project', ['domain_id'])


    models = {
        u'domains.domain': {
            'Meta': {'ordering': "['name']", 'object_name': 'Domain'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_root': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('libs.fields.DomainNameField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'projects.package': {
            'Meta': {'ordering': "['package']", 'object_name': 'Package'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'package': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'project': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['projects.Project']"}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'projects.project': {
            'Meta': {'ordering': "['name']", 'object_name': 'Project'},
            'directory': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'domain': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['domains.Domain']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'nginx_enable_access_log': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'nginx_enable_error_log': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'nginx_port': ('django.db.models.fields.PositiveIntegerField', [], {'default': '80'}),
            'nginx_static_root': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'nginx_static_subdomain': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'nginx_static_url': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': u"orm['users.LinuxUser']", 'null': 'True'}),
            'uwsgi_buffer_size': ('django.db.models.fields.PositiveIntegerField', [], {'default': '32768'}),
            'uwsgi_harakiri': ('django.db.models.fields.PositiveIntegerField', [], {'default': '30'}),
            'uwsgi_max_requests': ('django.db.models.fields.PositiveIntegerField', [], {'default': '5000'}),
            'uwsgi_settings_path': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'})
        },
        u'users.linuxgroup': {
            'Meta': {'ordering': "['name']", 'object_name': 'LinuxGroup'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_system': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'users.linuxuser': {
            'Meta': {'ordering': "['username']", 'object_name': 'LinuxUser'},
            'default_group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.LinuxGroup']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_system': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'salt': ('django.db.models.fields.CharField', [], {'default': "'salt33'", 'max_length': '6'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        }
    }

    complete_apps = ['projects']