# -*- coding: utf-8 -*-
from django.conf.urls import *

urlpatterns = patterns('apps.projects.views',
    url(regex=r'^$', view='project_list', name='all'),
    url(regex=r'^add/$', view='project_ac', name='add_project'),
    url(regex=r'^(?P<project_id>\d+)/$', view='project_detail', name='detail'),
    url(regex=r'^(?P<project_id>\d+)/packages/$', view='project_packages', name='project_packages'),
    url(regex=r'^(?P<project_id>\d+)/nginx/$', view='project_nginx', name='project_nginx'),
    url(regex=r'^(?P<project_id>\d+)/uwsgi/$', view='project_uwsgi', name='project_uwsgi'),

    #url(regex=r'^(?P<project_id>\d+)/delete/$', view='project_delete', name='delete_project'),

    # Subrocesses
    url(regex=r'^subprocess/create_virtualenv/$', view='create_virtualenv', name='create_virtualenv'),
    url(regex=r'^subprocess/install_packages/$', view='install_packages', name='install_packages'),
    url(regex=r'^subprocess/setup_nginx/$', view='setup_nginx', name='setup_nginx'),
    url(regex=r'^subprocess/create_user/$', view='create_user', name='create_user'),

)