# -*- coding: utf-8 -*-
import subprocess
import os
import json
import time

from django.shortcuts import get_object_or_404
from django.template import RequestContext
from django.template.loader import get_template
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.conf import settings

from lesshms.paths import VIRTUAL_ENV

from apps.projects.models import Project, Package
from apps.projects.forms import ProjectForm, NewPackageForm, NignxForm, UwsgiForm
from apps.users.models import LinuxUser, LinuxGroup


@login_required
def project_list(request):
    projects = Project.objects.all()

    template = get_template("projects/project_list.html")
    context = RequestContext(request, {
        "mm_item": "projects",
        "html_title": u"Projects",
        "projects": projects
    })
    return HttpResponse(template.render(context))


@login_required
def project_detail(request, project_id):
    project = get_object_or_404(Project, id=project_id)

    template = get_template("projects/project_detail.html")
    context = RequestContext(request, {
        "mm_item": "projects",
        "dm_item": "base",
        "html_title": u"Project %s" % project.name,
        "project": project,

        "breadcrumbs": [
            {"name": u"Projects", "url": reverse("projects:all")},
        ],
    })
    return HttpResponse(template.render(context))


@login_required
def project_packages(request, project_id):
    project = get_object_or_404(Project, id=project_id)
    packages = Package.objects.filter(project=project)

    form = NewPackageForm()

    template = get_template("projects/project_packages.html")
    context = RequestContext(request, {
        "mm_item": "projects",
        "dm_item": "packages",
        "html_title": u"Project packages",

        "project": project,
        "packages": packages,
        "form": form,

        "breadcrumbs": [
            {"name": u"Projects", "url": reverse("projects:all")},
            {"name": project.name, "url": reverse("projects:detail", kwargs={"project_id": project.id})},
        ],
    })
    return HttpResponse(template.render(context))


@login_required
def project_nginx(request, project_id):
    project = get_object_or_404(Project, id=project_id)
    packages = Package.objects.filter(project=project)

    nginx_fields = ["nginx_port", "nginx_enable_access_log", "nginx_enable_error_log",
                    "nginx_static_root", "nginx_static_subdomain", "nginx_static_url"]

    nginx_data = {}
    for field in nginx_fields:
        nginx_data[field] = getattr(project, field)

    form = NignxForm(request.POST or nginx_data)

    if request.method == "POST":
        if form.is_valid():
            for field in nginx_fields:
                setattr(project, field, form.cleaned_data[field])

            project.save()
            project.save_nginx_config()

            messages.info(request, 'Nginx config changed.')
            return HttpResponseRedirect(reverse("projects:project_nginx", kwargs={
                "project_id": project.id
            }))

    template = get_template("projects/project_nginx.html")
    context = RequestContext(request, {
        "mm_item": "projects",
        "dm_item": "nginx",
        "html_title": u"Nginx settings",

        "project": project,
        "packages": packages,
        "form": form,

        "breadcrumbs": [
            {"name": u"Projects", "url": reverse("projects:all")},
            {"name": project.name, "url": reverse("projects:detail", kwargs={"project_id": project.id})},
        ],
    })
    return HttpResponse(template.render(context))


@login_required
def project_uwsgi(request, project_id):
    project = get_object_or_404(Project, id=project_id)
    packages = Package.objects.filter(project=project)

    uwsgi_fields = ["uwsgi_settings_path", "uwsgi_max_requests", "uwsgi_buffer_size", "uwsgi_harakiri"]

    uwsgi_data = {}
    for field in uwsgi_fields:
        uwsgi_data[field] = getattr(project, field)

    form = UwsgiForm(request.POST or uwsgi_data)

    if request.method == "POST":
        if form.is_valid():
            for field in uwsgi_fields:
                setattr(project, field, form.cleaned_data[field])

            project.save()
            project.save_uwsgi_config()

            messages.info(request, 'Uwsgi config changed.')
            return HttpResponseRedirect(reverse("projects:project_uwsgi", kwargs={
                "project_id": project.id
            }))

    template = get_template("projects/project_uwsgi.html")
    context = RequestContext(request, {
        "mm_item": "projects",
        "dm_item": "uwsgi",
        "html_title": u"uwsgi settings",

        "project": project,
        "packages": packages,
        "form": form,

        "breadcrumbs": [
            {"name": u"Projects", "url": reverse("projects:all")},
            {"name": project.name, "url": reverse("projects:detail", kwargs={"project_id": project.id})},
        ]
    })
    return HttpResponse(template.render(context))


@login_required
def project_ac(request):

    form = ProjectForm()

    if request.method == "POST" and request.is_ajax():
        directory = Project.normalize_directory_name(request.POST.get("directory", ""))
        name = request.POST.get("name", "").strip()
        domain_id = request.POST.get("domain_id")
        user_id = request.POST.get("user_id", '')

        if directory and name:
            project = Project(
                name=name,
                directory=directory,
                domain_id=domain_id,
                user_id=user_id or None
            )

            project.save()

            data = {
                "id": project.id,
                "name": project.name,
                "directory": project.directory,
                "user_id": project.user.id if project.user else '',
                "status": "success"
            }
        else:
            data = {
                "status": "error",
                "error": u"Bad name or directory."
            }

        time.sleep(3)
        return HttpResponse(json.dumps(data), mimetype="application/json")

    template = get_template("projects/project_ac.html")
    context = RequestContext(request, {
        "mm_item": "projects",
        "html_title": u"Create project",
        "form": form,

        "breadcrumbs": [
            {"name": u"Projects", "url": reverse("projects:all")},
        ],

    })
    return HttpResponse(template.render(context))

############################
# * * * Subprocesses * * * #
############################


@login_required
def create_virtualenv(request):
    project_id = request.POST.get("project_id", None)
    project = get_object_or_404(Project, id=project_id)

    # Create virtualenv
    if not settings.DEVELOP:
        command = ["su", "-", project.user.username, "-c",  '%s %s' % (VIRTUAL_ENV, project.get_virtualenv_dir())]
    else:
        command = [VIRTUAL_ENV, project.get_virtualenv_dir()]

    p = subprocess.Popen(command,
                         bufsize=1000 * 1000,
                         stdin=subprocess.PIPE,
                         stderr=subprocess.PIPE,
                         stdout=subprocess.PIPE)

    stdout, stderr = p.communicate()

    if stderr:
        data = {
            "status": "error",
            "error": stderr
        }
    else:
        data = {
            "status": "success"
        }

    return HttpResponse(json.dumps(data), mimetype="application/json")


@login_required
def install_packages(request):
    project_id = request.POST.get("project_id", None)
    project = get_object_or_404(Project, id=project_id)

    package_name = request.POST.get("package_name", "").strip()
    package_version = request.POST.get("package_version", "").strip()

    pip = os.path.join(project.get_virtualenv_dir(), "bin/pip")

    create = upgrade = False

    try:
        package = Package.objects.get(project=project, package=package_name)
        if package.version != package_version:
            upgrade = True
    except Package.DoesNotExist:
        create = True
        package = Package(project=project)

    if (create or upgrade) and package_name and package_version:

        command = []
        if settings.DEVELOP:
            if create:
                command = [pip, "install", "%s==%s" % (package_name, package_version)]
            if upgrade:
                command = [pip, "install", "--upgrade", "%s==%s" % (package_name, package_version)]
        else:
            if create:
                command = ["su", "-", project.user.username, "-c", "%s %s %s==%s" % (
                    pip, "install", package_name, package_version)]
            if upgrade:
                command = ["su", "-", project.user.username, "-c", "%s %s %s %s==%s" % (
                    pip, "install", "--upgrade", package_name, package_version)]

        p = subprocess.Popen(command,
                             bufsize=1000 * 1000,
                             stdin=subprocess.PIPE,
                             stderr=subprocess.PIPE,
                             stdout=subprocess.PIPE)
        stdout, stderr = p.communicate()

        package.package = package_name
        package.version = package_version
        package.save()

    return HttpResponse(json.dumps(""), mimetype="application/json")


@login_required
def setup_nginx(request):
    project_id = request.POST.get("project_id", None)
    project = get_object_or_404(Project, id=project_id)

    project.save_nginx_config()

    time.sleep(3)
    return HttpResponse(json.dumps(""), mimetype="application/json")


@login_required
def create_user(request):
    project_id = request.POST.get("project_id", None)
    project = get_object_or_404(Project, id=project_id)

    username = request.POST.get("username", "")
    password = request.POST.get("password", "")

    data = {
        "status": "success"
    }

    if username and password:
        group = LinuxGroup(name=username)
        group.save()
        group.create_system_group()

        user = LinuxUser(username=username, default_group=group)
        user.save()
        user.create_system_user(password)

        project.user = user
        project.save()

        # TODO: Errors for create_system_*

    time.sleep(3)
    return HttpResponse(json.dumps(data), mimetype="application/json")