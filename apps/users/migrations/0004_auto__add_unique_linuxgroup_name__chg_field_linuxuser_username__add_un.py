# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding unique constraint on 'LinuxGroup', fields ['name']
        db.create_unique(u'users_linuxgroup', ['name'])


        # Changing field 'LinuxUser.username'
        db.alter_column(u'users_linuxuser', 'username', self.gf('django.db.models.fields.CharField')(unique=True, max_length=32))
        # Adding unique constraint on 'LinuxUser', fields ['username']
        db.create_unique(u'users_linuxuser', ['username'])


    def backwards(self, orm):
        # Removing unique constraint on 'LinuxUser', fields ['username']
        db.delete_unique(u'users_linuxuser', ['username'])

        # Removing unique constraint on 'LinuxGroup', fields ['name']
        db.delete_unique(u'users_linuxgroup', ['name'])


        # Changing field 'LinuxUser.username'
        db.alter_column(u'users_linuxuser', 'username', self.gf('django.db.models.fields.CharField')(max_length=100))

    models = {
        u'users.linuxgroup': {
            'Meta': {'ordering': "['name']", 'object_name': 'LinuxGroup'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_system': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        },
        u'users.linuxuser': {
            'Meta': {'ordering': "['username']", 'object_name': 'LinuxUser'},
            'default_group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.LinuxGroup']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_system': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'salt': ('django.db.models.fields.CharField', [], {'default': "'salt33'", 'max_length': '6'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'})
        }
    }

    complete_apps = ['users']