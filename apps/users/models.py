# -*- coding: utf-8 -*-
import os
import crypt
import subprocess

from django.db import models
from django.conf import settings

from lesshms.server_conf import GID_START, UID_START, SHELL
from lesshms.paths import HOME

from lesshms.paths import CMD_USERADD, CMD_GROUPADD


class LinuxGroup(models.Model):
    name = models.CharField(u"Name", max_length=100, unique=True)
    is_system = models.BooleanField(default=False)

    class Meta:
        ordering = ["name"]

    def __unicode__(self):
        return self.name

    def gid(self):
        return str(GID_START + self.id)

    def create_system_group(self):
        if not self.is_system and not settings.DEVELOP:

            command = [CMD_GROUPADD, "-g", self.gid(), self.name]

            p = subprocess.Popen(command,
                                 bufsize=1000 * 1000,
                                 stdin=subprocess.PIPE,
                                 stderr=subprocess.PIPE,
                                 stdout=subprocess.PIPE)

            while True:
                next_line = p.stdout.readline()
                if next_line == '' and p.poll() is not None:
                    break

            self.is_system = True
            self.save()


class LinuxUser(models.Model):
    default_group = models.ForeignKey(LinuxGroup)
    username = models.CharField(u"Name", max_length=32, unique=True)

    # TODO: need salt generator
    salt = models.CharField(max_length=6, default="salt33")

    is_system = models.BooleanField(default=False)

    class Meta:
        ordering = ["username"]

    def __unicode__(self):
        return self.username

    def uid(self):
        return str(UID_START + self.id)

    def gid(self):
        return self.default_group.gid()

    def get_home_dir(self):
        return os.path.join(HOME, self.username)

    def create_system_user(self, password):
        if not self.is_system and not settings.DEVELOP:

            enc_password = crypt.crypt(password, self.salt)

            command = [
                CMD_USERADD,
                "-d", self.get_home_dir(),
                "-g", self.gid(),
                "-p", enc_password,
                "-u", self.uid(),
                "-s", SHELL,
                "--create-home",
                self.username
            ]

            p = subprocess.Popen(command,
                                 bufsize=1000 * 1000,
                                 stdin=subprocess.PIPE,
                                 stderr=subprocess.PIPE,
                                 stdout=subprocess.PIPE)

            #stdout, stderr = p.communicate()

            self.is_system = True
            self.save()
        else:
            os.makedirs(self.get_home_dir())
