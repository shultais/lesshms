# -*- coding: utf-8 -*-
import time

from django.template import RequestContext
from django.template.loader import get_template
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.contrib import messages

from apps.users.models import LinuxUser, LinuxGroup
from apps.users.forms import NewUserForm


@login_required
def user_list(request):
    users = LinuxUser.objects.all()

    template = get_template("users/user_list.html")
    context = RequestContext(request, {
        "mm_item": "users",
        "html_title": u"Linux Users",
        "users": users
    })
    return HttpResponse(template.render(context))


@login_required
def user_add(request):
    form = NewUserForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            # Group
            if form.cleaned_data["create_group"]:
                group = LinuxGroup(
                    name=form.cleaned_data["group_name"] or form.cleaned_data["username"]
                )
                group.save()

            else:
                group = form.cleaned_data["group"]
                if not group:
                    group = LinuxGroup(
                        name=form.cleaned_data["username"]
                    )
                    group.save()

            group.create_system_group()
            # TODO: Bad code, need wait group.create_system_group()
            time.sleep(2)

            # User
            if group:
                user = LinuxUser(
                    username=form.cleaned_data["username"],
                    default_group=group
                )
                user.save()
                user.create_system_user(form.cleaned_data["password"])
                messages.info(request, 'User %s is created.' % user.username)

            else:
                # TODO: Need more of a logic
                pass
            return HttpResponseRedirect(reverse("users:user_list"))

    template = get_template("users/user_add.html")
    context = RequestContext(request, {
        "mm_item": "users",
        "html_title": u"Add Linux Users",

        "form": form,

        "breadcrumbs": [
            {"name": u"Users", "url": reverse("users:user_list")},
        ],

    })
    return HttpResponse(template.render(context))

