# -*- coding: utf-8 -*-
import re

from django.core.validators import RegexValidator, ValidationError
from django.utils.encoding import smart_unicode


# From https://code.djangoproject.com/ticket/18119
class DomainNameValidator(RegexValidator):
    # from URLValidator + there can be most 127 labels (at most 255 total chars)
    regex = re.compile(
        r'^(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.){0,126}(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?))$',
        re.IGNORECASE
    )
    message = 'Enter a valid domain name value'

    def __init__(self, *args, **kwargs):
        self.accept_idna = bool(kwargs.pop('accept_idna', True))
        super(DomainNameValidator, self).__init__(*args, **kwargs)
        if self.accept_idna:
            self.message = 'Enter a valid plain or internationalized domain name value'

    def __call__(self, value):
        # validate
        try:
            super(DomainNameValidator, self).__call__(value)
        except ValidationError as e:
            # maybe this is a unicode-encoded IDNA string?
            if not self.accept_idna:
                raise
            if not value:
                raise

            # convert it unicode -> ascii
            try:
                asciival = smart_unicode(value).encode('idna')
            except UnicodeError:
                # raise the original ASCII error
                raise e
            # validate the ascii encoding of it
        super(DomainNameValidator, self).__call__(asciival)