# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.db.models.fields import CharField

from libs.validators import DomainNameValidator


# From https://code.djangoproject.com/ticket/18120
class DomainNameField(CharField):
    """An Internationalized Domain Name in ASCII or UTF format, stored as idna-encoded (ASCII) string."""
    description = _("Internet Domain Name")

    def __init__(self, *args, **kwargs):
        accept_idna = kwargs.pop('accept_idna', True)
        kwargs['max_length'] = 255
        super(CharField, self).__init__(*args, **kwargs)
        self.validators.append(DomainNameValidator(accept_idna=accept_idna))

    def to_python(self, value):
        # we always store domain names in ASCII format
        value = super(DomainNameField, self).to_python(value)
        # make sure we got called on RAW data, because django calls to_python()
        # even on already-python data. IDNA-decoding UTF would raise exception.
        if value.encode('idna') != value:   # value is UTF (python) already
            return value
        # value is RAW or python-ASCII, safe to encode in either case
        return value.decode('idna')

    def get_prep_value(self, value):
        # we always store domain names in ASCII format
        return value.encode('idna')

from south.modelsinspector import add_introspection_rules
add_introspection_rules([], ["^libs\.fields\.DomainNameField"])